import React from "react";
import {Link} from "react-router-dom";
import "./css/packageBox.css";
import {PACKAGE_IMG} from "../constants/IMGURL.constant";

export function PackageBox(props) {
    return (
        <>
            {props.packageList.length !== 0 ?
                <div className="packageBox-data-wrapper">
                    <h1>{props.boxTitle}</h1>
                    {props.packageList.map((ele, i) => {
                        if (i < 6) {
                            return (
                                <div className="packageBox-data" key={i}>
                                    <div
                                        className="packageBox-detail"
                                        onClick={() => {
                                            props.history.push(`/package/${ele._id}`);
                                        }}
                                    >
                                        <h2>{ele.packageName}</h2>
                                        <h5>
                                            USD {ele.tripFacts.price} | {ele.tripFacts.duration - 1}{" "}
                                            Nights {ele.tripFacts.duration} Days
                                        </h5>
                                        <p>{ele.description}</p>
                                        <Link to="/">More Info...</Link>
                                    </div>
                                    <img
                                        src={`${PACKAGE_IMG}/${ele.image}`}
                                        alt="temp-use2.jpg"
                                    />
                                </div>
                            );
                        } else {
                            return null;
                        }
                    })}
                </div>
                : null}</>
    );
}
