const BASE_IMG_URL = 'http://localhost:2020/file/image/';

export const OUR_TEAM_IMG = `${BASE_IMG_URL}ourTeamIMG`;
export const LEGAL_DOCS_IMG = `${BASE_IMG_URL}legalDocsIMG`;
export const PACKAGE_IMG = `${BASE_IMG_URL}packageIMG`;
export const ADVENTURE_ACT_IMG = `${BASE_IMG_URL}advActIMG`;
export const COMPANY_LOGO = `${BASE_IMG_URL}/defaultIMG/COMPANY-LOGO.png`

export const DEFAULT = {
    TREK_IMG: `${BASE_IMG_URL}defaultIMG/TREKKING.png`,
    TOUR_IMG: `${BASE_IMG_URL}defaultIMG/TOUR.jpg`
}