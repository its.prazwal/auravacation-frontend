import React from "react";
import {
    BrowserRouter as Router,
    Route,
    Switch,
} from "react-router-dom";
import {Homepage} from "./components/homepage/homepage.component";
import {HeadBar} from "./components/header/header.navbar";
import FooterBar from "./components/footer/footer";
import StylePageComponent from "./components/stylePage/stylePage.component";
import PackagePage from "./components/packagePage/package.component";
import AboutUs from "./components/companyProfile/AboutUs";
import ContactUs from "./components/companyProfile/ContactUs";
import LegalDocs from "./components/companyProfile/LegalDocs";
import OurTeam from "./components/companyProfile/OurTeam";
import PrivacyPolicy from "./components/companyProfile/PrivacyPolicy";
import WhyCompany from "./components/companyProfile/WhyCompany";

class Notfound extends React.Component {
    render() {
        return (
            <div>
                <h3>Page not found</h3>
                <p>Error: 404</p>
            </div>
        );
    }
}

function scrollToTop() {
    const c = document.documentElement.scrollTop || document.body.scrollTop;
    if (c > 0) {
        window.requestAnimationFrame(scrollToTop);
        window.scrollTo(0, c - c / 15);
    }
};

const PageRoute = ({component: Component, ...data}) => {
    return (
        <Route
            {...data}
            render={(props) => {
                scrollToTop();
                return (
                    <>
                        <div className="HeadBar disableTextSelection">
                            <HeadBar {...props} />
                        </div>
                        <div className="Components disableTextSelection">
                            <Component {...props} />
                        </div>
                        <div className="FooterBar">
                            <FooterBar {...props} />
                        </div>
                    </>
                );
            }}
        />
    );
};

let ComponentRouter = () => {
    return (
        <Router>
            <Switch>
                <PageRoute exact path="/" component={Homepage}/>
                <PageRoute
                    exact
                    path="/style/:name"
                    component={StylePageComponent}
                />
                <PageRoute
                    exact
                    path="/package/:id"
                    component={PackagePage}
                />
                <PageRoute exact path={'/About-Us'} component={AboutUs}/>
                <PageRoute exact path={'/Contact-Us'} component={ContactUs}/>
                <PageRoute exact path={'/Legal-Documents'} component={LegalDocs}/>
                <PageRoute exact path={'/Our-Team'} component={OurTeam}/>
                <PageRoute exact path={'/Privacy-Policy'} component={PrivacyPolicy}/>
                <PageRoute exact path={'/Why-Aura-Vacation'} component={WhyCompany}/>
                <PageRoute component={Notfound}/>
            </Switch>
        </Router>
    );
};

export default ComponentRouter;
