const ADVENTURE_ACT = [];
const PACKAGE = [];
const REVIEWS = [];
const USEFUL_INFO = [];
const CUSTOMERS = [];

export function AdventureActReducer(state = ADVENTURE_ACT, action) {
    switch (action.type) {
        case 'SAVE_ADVENTURE_ACT':
            return action.payload;
        default:
            return state;
    }
}

export function PackageReducer(state = PACKAGE, action) {
    switch (action.type) {
        case 'SAVE_PACKAGE':
            return action.payload;
        default:
            return state;
    }
}

export function ReviewsReducer(state = REVIEWS, action) {
    switch (action.type) {
        case 'SAVE_REVIEWS':
            return action.payload;
        default:
            return state;
    }
}

export function CustomerReducer(state = CUSTOMERS, action) {
    switch (action.type) {
        case 'SAVE_CUSTOMER':
            return action.payload;
        case 'GET_CUSTOMER_BY_ID':
            return state.filter(s => s._id === action.payload)[0];
        default:
            return state;
    }
}

export function UsefulInfoReducer(state = USEFUL_INFO, action) {
    switch (action.type) {
        case 'SAVE_USEFUL_INFO':
            return action.payload;
        default:
            return state;
    }
}
