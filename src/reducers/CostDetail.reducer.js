const COST_DETAILS = {
    EXCLUDES: [],
    INCLUDES: [],
}

function CostDetailsReducer(state = COST_DETAILS, action){
    switch (action.type) {
        case 'SAVE_EXCLUDES':
            return {...state, EXCLUDES: action.payload};
        case 'GET_EXCLUDES':
            return state.EXCLUDES;
        case 'SAVE_INCLUDES':
            return {...state, INCLUDES: action.payload};
        case 'GET_INCLUDES':
            return state.INCLUDES;
        default:
            return state;
    }
}
export default CostDetailsReducer;