const LOCATION = {
    COUNTRY: [],
    PLACES: [],
    REGION: [],
}

function LocationReducer(state = LOCATION, action){
    switch (action.type) {
        case 'SAVE_COUNTRY':
            return {...state, COUNTRY: action.payload};
        case 'GET_COUNTRY':
            return state.COUNTRY;
        case 'SAVE_PLACES':
            return {...state, PLACES: action.payload};
        case 'GET_PLACES':
            return state.PLACES;
        case 'SAVE_REGION':
            return {...state, REGION: action.payload};
        case 'GET_REGION':
            return state.REGION
        default:
            return state;
    }
}

export default LocationReducer;