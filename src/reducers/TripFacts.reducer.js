const TRIP_FACTS = {
    ACCOMMODATION: [],
    HIGHLIGHTS: [],
    STYLES: [],
    TRANSPORTATION: [],
};

function TripFactsReducer(state = TRIP_FACTS, action){
    switch (action.type) {
        case 'SAVE_ACCOMMODATION':
            return {...state, ACCOMMODATION: action.payload};
        case 'GET_ACCOMMODATION':
            return state.ACCOMMODATION;
        case 'SAVE_HIGHLIGHTS':
            return {...state, HIGHLIGHTS: action.payload};
        case 'GET_HIGHLIGHTS':
            return state.HIGHLIGHTS;
        case 'SAVE_STYLES':
            return {...state, STYLES: action.payload};
        case 'GET_STYLES':
            return state.STYLES;
        case 'SAVE_TRANSPORTATION':
            return {...state, TRANSPORTATION: action.payload};
        case 'GET_TRANSPORTATION':
            return state.TRANSPORTATION;
        default:
            return state;
    }
}

export default TripFactsReducer;