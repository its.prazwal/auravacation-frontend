const layouts = {
    HOMEPAGE: [],
    FOOTER: [],
};

function layoutReducer(state = layouts, action) {
    switch (action.type) {
        case 'SAVE_HP_LAYOUT':
            return {...state, HOMEPAGE: action.payload};
        case 'GET_HP_LAYOUT':
            return layouts.HOMEPAGE;
        case 'SAVE_FT_LAYOUT':
            return {...state, FOOTER: action.payload};
        case 'GET_FT_LAYOUT':
            return layouts.FOOTER;
        default:
            return state;
    }
}

export default layoutReducer;