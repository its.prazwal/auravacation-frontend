const COMPANY = {
    ABOUT_US: [],
    CONTACT_DETAIL: [],
    LEGAL_DOCS: [],
    OUR_TEAM: [],
    PRIVACY_POLICY: [],
    WHY_COMPANY: [],
}

function CompanyReducer(state = COMPANY, action) {
    switch (action.type) {
        case 'SAVE_ABOUT_US':
            return {...state, ABOUT_US: action.payload};
        case 'GET_ABOUT_US':
            return state.ABOUT_US
        case 'SAVE_CONTACT_DETAIL':
            return {...state, CONTACT_DETAIL: action.payload};
        case 'GET_CONTACT_DETAIL':
            return state.CONTACT_DETAIL
        case 'SAVE_LEGAL_DOCS':
            return {...state, LEGAL_DOCS: action.payload};
        case 'GET_LEGAL_DOCS':
            return state.LEGAL_DOCS
        case 'SAVE_OUR_TEAM':
            return {...state, OUR_TEAM: action.payload};
        case 'GET_OUR_TEAM':
            return state.OUR_TEAM
        case 'SAVE_PRIVACY_POLICY':
            return {...state, PRIVACY_POLICY: action.payload};
        case 'GET_PRIVACY_POLICY':
            return state.PRIVACY_POLICY
        case 'SAVE_WHY_COMPANY':
            return {...state, WHY_COMPANY: action.payload};
        case 'GET_WHY_COMPANY':
            return state.WHY_COMPANY
        default:
            return state;
    }
}

export default CompanyReducer;