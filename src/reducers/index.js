import {combineReducers} from 'redux';

//IMPORTING ALL OTHER REDUCERS
import CompanyReducer from "./Company.reducer";
import CostDetailsReducer from "./CostDetail.reducer";
import layoutReducer from "./Layout.reducer";
import LocationReducer from "./Location.reducer";
import TripFactsReducer from "./TripFacts.reducer";
import * as Other from './Other.reducer';

const allReducers = combineReducers({
    company: CompanyReducer,
    costDetail: CostDetailsReducer,
    layouts: layoutReducer,
    location: LocationReducer,
    tripFacts: TripFactsReducer,
    adventureAct: Other.AdventureActReducer,
    package: Other.PackageReducer,
    reviews: Other.ReviewsReducer,
    usefulInfo: Other.UsefulInfoReducer,
})

export default allReducers;