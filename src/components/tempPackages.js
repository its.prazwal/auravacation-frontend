export const tempPackage = [
  {
    id: 639472048205,
    title: "Dhankuta Dharan Tour",
    country: "Nepal",
    places: ["Dharan", "Dhankuta", "Hile"],
    style: "Tour",
    region: null,
    tripFacts: {
      duration: 7,
      grade: "Easy",
      transportation: ["Private Vehicle", "Domestic Flight"],
      accommodation: ["Hotel"],
      price: 899,
      altitude: 2000,
      season: [1, 2, 3],
      meals: ["Breakfast", "Lunch", "Dinner"],
    },
    highlights: [
      "this is highlight no 1. Nepal",
      "this is highlight no 2. kathmandu",
      "this is highlight no 3. nepal",
      "this is highlight no 4. pokhara",
    ],
    packageDescription:
      "Tour hkhflk Tour lksdjf oirehf Tour ireubf kanfieousd kjf n;tour ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
    image: "temp-image1.jpg",
    itinerary: [
      {
        day: 1,
        itineraryTitle: "itinerary 1",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 2,
        itineraryTitle: "itinerary 2",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 3,
        itineraryTitle: "itinerary 3",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 4,
        itineraryTitle: "itinerary 4",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 5,
        itineraryTitle: "itinerary 5",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
    ],
    costDetailsList: {
      costIncludes: [1, 3, 5, 4, 6, 7],
      costExcludes: [2, 4, 6, 7, 8],
    },
    usefulInformationList: [1, 4, 2, 5, 6, 7],
    reviewsList: [1, 2, 4, 5],
    displayOption: false,
  },
  {
    id: 732849629412,
    title: "Everest base camp trek",
    country: "Nepal",
    places: ["Everest", "Everest2", "Everest2"],
    style: "Trek",
    region: "Everest",
    tripFacts: {
      duration: 10,
      grade: "Moderate",
      transportation: ["Private Vehicle", "Domestic Flight"],
      accommodation: ["Hotel", "Tea House"],
      price: 1099,
      altitude: 5500,
      season: [6, 7, 8],
      meals: ["Breakfast", "Lunch", "Dinner"],
    },
    highlights: [
      "this is highlight no 1.",
      "this is highlight no 2.",
      "this is highlight no 3.",
      "this is highlight no 4.",
    ],
    packageDescription:
      "fiouidkjbmnfkwj hkhflk flaskjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
    image: "temp-image2.jpg",
    itinerary: [
      {
        day: 1,
        itineraryTitle: "itinerary 1",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 2,
        itineraryTitle: "itinerary 2",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 3,
        itineraryTitle: "itinerary 3",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 4,
        itineraryTitle: "itinerary 4",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 5,
        itineraryTitle: "itinerary 5",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 6,
        itineraryTitle: "itinerary 6",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 7,
        itineraryTitle: "itinerary 7",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 8,
        itineraryTitle: "itinerary 8",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 9,
        itineraryTitle: "itinerary 9",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 10,
        itineraryTitle: "itinerary 10",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
    ],
    costDetailsList: {
      costIncludes: [2, 5, 6, 1, 3, 7],
      costExcludes: [1, 3, 7, 9, 5],
    },
    usefulInformationList: [1, 4, 2, 5, 6, 7],
    reviewsList: [3, 6, 7],
    displayOption: true,
  },
  {
    id: 456789098765,
    title: "Ktm Tour",
    country: "Nepal",
    places: ["Kathmandu"],
    style: "Tour",
    region: null,
    tripFacts: {
      duration: 3,
      grade: "Easy",
      transportation: ["Private Vehicle", "Domestic Flight"],
      accommodation: ["Hotel"],
      price: 399,
      altitude: 1800,
      season: [9, 10],
      meals: ["Breakfast", "Lunch", "Dinner"],
    },
    highlights: [
      "this is highlight no 1.",
      "this is highlight no 2.",
      "this is highlight no 3.",
      "this is highlight no 4.",
    ],
    packageDescription:
      "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
    image: "temp-image3.jpg",
    itinerary: [
      {
        day: 1,
        itineraryTitle: "itinerary 1",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 2,
        itineraryTitle: "itinerary 2",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 3,
        itineraryTitle: "itinerary 3",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
    ],
    costDetailsList: {
      costIncludes: [1, 3, 5, 7, 9],
      costExcludes: [2, 4, 6, 7, 8],
    },
    usefulInformationList: [1, 2],
    reviewsList: [8, 9],
    displayOption: true,
  },
  {
    id: 738957637592,
    title: "KTM PKR trek",
    country: "Nepal",
    places: ["Kathmandu", "Pokhara"],
    style: "Tour",
    region: null,
    tripFacts: {
      duration: 5,
      grade: "Easy",
      transportation: ["Private Vehicle", "Domestic Flight"],
      accommodation: ["Hotel"],
      price: 599,
      altitude: 2000,
      season: [1, 2, 3],
      meals: ["Breakfast", "Lunch", "Dinner"],
    },
    highlights: [
      "this is highlight no 1. Nepal",
      "this is highlight no 2. kathmandu",
      "this is highlight no 3. nepal",
      "this is highlight no 4. pokhara",
    ],
    packageDescription:
      " hkhflk  lksdjf oirehf  ireubf kanfieousd kjf n; ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
    image: "temp-image2.jpg",
    itinerary: [
      {
        day: 1,
        itineraryTitle: "itinerary 1",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 2,
        itineraryTitle: "itinerary 2",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 3,
        itineraryTitle: "itinerary 3",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 4,
        itineraryTitle: "itinerary 4",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 5,
        itineraryTitle: "itinerary 5",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
    ],
    costDetailsList: {
      costIncludes: [1, 3, 5, 4, 6, 7],
      costExcludes: [2, 4, 6, 7, 8],
    },
    usefulInformationList: [1, 4, 2, 5, 6, 7],
    reviewsList: [1, 2, 4, 5],
    displayOption: false,
  },
  {
    id: 639502759264,
    title: "Everest base camp trek",
    country: "Nepal",
    places: ["Everest", "Everest2", "Everest2"],
    style: "Trek",
    region: "Everest",
    tripFacts: {
      duration: 10,
      grade: "Moderate",
      transportation: ["Private Vehicle", "Domestic Flight"],
      accommodation: ["Hotel", "Tea House"],
      price: 1099,
      altitude: 5500,
      season: [6, 7, 8],
      meals: ["Breakfast", "Lunch", "Dinner"],
    },
    highlights: [
      "this is highlight no 1.",
      "this is highlight no 2.",
      "this is highlight no 3.",
      "this is highlight no 4.",
    ],
    packageDescription:
      "fiouidkjbmnfkwj hkhflk flaskjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
    image: "temp-image3.jpg",
    itinerary: [
      {
        day: 1,
        itineraryTitle: "itinerary 1",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 2,
        itineraryTitle: "itinerary 2",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 3,
        itineraryTitle: "itinerary 3",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 4,
        itineraryTitle: "itinerary 4",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 5,
        itineraryTitle: "itinerary 5",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 6,
        itineraryTitle: "itinerary 6",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 7,
        itineraryTitle: "itinerary 7",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 8,
        itineraryTitle: "itinerary 8",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 9,
        itineraryTitle: "itinerary 9",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 10,
        itineraryTitle: "itinerary 10",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
    ],
    costDetailsList: {
      costIncludes: [2, 5, 6, 1, 3, 7],
      costExcludes: [1, 3, 7, 9, 5],
    },
    usefulInformationList: [1, 4, 2, 5, 6, 7],
    reviewsList: [3, 6, 7],
    displayOption: true,
  },
  {
    id: 321895629732,
    title: "Ktm sdsdf",
    country: "Nepal",
    places: ["Kathmandu"],
    style: "Trek",
    region: null,
    tripFacts: {
      duration: 3,
      grade: "Easy",
      transportation: ["Private Vehicle", "Domestic Flight"],
      accommodation: ["Hotel"],
      price: 399,
      altitude: 1800,
      season: [9, 10],
      meals: ["Breakfast", "Lunch", "Dinner"],
    },
    highlights: [
      "this is highlight no 1.",
      "this is highlight no 2.",
      "this is highlight no 3.",
      "this is highlight no 4.",
    ],
    packageDescription:
      "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
    image: "temp-image1.jpg",
    itinerary: [
      {
        day: 1,
        itineraryTitle: "itinerary 1",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 2,
        itineraryTitle: "itinerary 2",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 3,
        itineraryTitle: "itinerary 3",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
    ],
    costDetailsList: {
      costIncludes: [1, 3, 5, 7, 9],
      costExcludes: [2, 4, 6, 7, 8],
    },
    usefulInformationList: [1, 2],
    reviewsList: [8, 9],
    displayOption: true,
  },
  {
    id: 357402746734,
    title: "KTM PKR Tour",
    country: "Nepal",
    places: ["Kathmandu", "Pokhara"],
    style: "Tour",
    region: null,
    tripFacts: {
      duration: 5,
      grade: "Easy",
      transportation: ["Private Vehicle", "Domestic Flight"],
      accommodation: ["Hotel"],
      price: 599,
      altitude: 2000,
      season: [1, 2, 3],
      meals: ["Breakfast", "Lunch", "Dinner"],
    },
    highlights: [
      "this is highlight no 1. Nepal",
      "this is highlight no 2. kathmandu",
      "this is highlight no 3. nepal",
      "this is highlight no 4. pokhara",
    ],
    packageDescription:
      "Tour hkhflk Tour lksdjf oirehf Tour ireubf kanfieousd kjf n;tour ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
    image: "temp-image1.jpg",
    itinerary: [
      {
        day: 1,
        itineraryTitle: "itinerary 1",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 2,
        itineraryTitle: "itinerary 2",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 3,
        itineraryTitle: "itinerary 3",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 4,
        itineraryTitle: "itinerary 4",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 5,
        itineraryTitle: "itinerary 5",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
    ],
    costDetailsList: {
      costIncludes: [1, 3, 5, 4, 6, 7],
      costExcludes: [2, 4, 6, 7, 8],
    },
    usefulInformationList: [1, 4, 2, 5, 6, 7],
    reviewsList: [1, 2, 4, 5],
    displayOption: false,
  },
  {
    id: 528472048232,
    title: "Everest base camp trek",
    country: "Nepal",
    places: ["Everest", "Everest2", "Everest2"],
    style: "Trek",
    region: "Everest",
    tripFacts: {
      duration: 10,
      grade: "Moderate",
      transportation: ["Private Vehicle", "Domestic Flight"],
      accommodation: ["Hotel", "Tea House"],
      price: 1099,
      altitude: 5500,
      season: [6, 7, 8],
      meals: ["Breakfast", "Lunch", "Dinner"],
    },
    highlights: [
      "this is highlight no 1.",
      "this is highlight no 2.",
      "this is highlight no 3.",
      "this is highlight no 4.",
    ],
    packageDescription:
      "fiouidkjbmnfkwj hkhflk flaskjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
    image: "temp-image3.jpg",
    itinerary: [
      {
        day: 1,
        itineraryTitle: "itinerary 1",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 2,
        itineraryTitle: "itinerary 2",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 3,
        itineraryTitle: "itinerary 3",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 4,
        itineraryTitle: "itinerary 4",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 5,
        itineraryTitle: "itinerary 5",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 6,
        itineraryTitle: "itinerary 6",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 7,
        itineraryTitle: "itinerary 7",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 8,
        itineraryTitle: "itinerary 8",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 9,
        itineraryTitle: "itinerary 9",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 10,
        itineraryTitle: "itinerary 10",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
    ],
    costDetailsList: {
      costIncludes: [2, 5, 6, 1, 3, 7],
      costExcludes: [1, 3, 7, 9, 5],
    },
    usefulInformationList: [1, 4, 2, 5, 6, 7],
    reviewsList: [3, 6, 7],
    displayOption: true,
  },
  {
    id: 652947204782,
    title: "Ktm Tour",
    country: "Nepal",
    places: ["Kathmandu"],
    style: "Tour",
    region: null,
    tripFacts: {
      duration: 3,
      grade: "Easy",
      transportation: ["Private Vehicle", "Domestic Flight"],
      accommodation: ["Hotel"],
      price: 399,
      altitude: 1800,
      season: ["Jan", "Feb"],
      meals: ["Breakfast", "Lunch", "Dinner"],
    },
    highlights: [
      "this is highlight no 1.",
      "this is highlight no 2.",
      "this is highlight no 3.",
      "this is highlight no 4.",
    ],
    packageDescription:
      "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
    image: "temp-image2.jpg",
    itinerary: [
      {
        day: 1,
        itineraryTitle: "itinerary 1",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 2,
        itineraryTitle: "itinerary 2",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
      {
        day: 3,
        itineraryTitle: "itinerary 3",
        itineraryDescription:
          "fiouidkjbmnfkwj hkhflk flaskjf lksdjf oirehf jnf ireubf kanfieousd kjf n;sddkmnf ; wfnoIF N;LMDNF;MSNDF L;KAWEH FILWEHFLKSJHFD ;KLJDSHFKJADHF;H",
      },
    ],
    costDetailsList: {
      costIncludes: [1, 3, 5, 7, 9],
      costExcludes: [2, 4, 6, 7, 8],
    },
    usefulInformationList: [1, 2],
    reviewsList: [8, 9],
    displayOption: true,
  },
];

export const tempCostDetails = {
  costExcludes: [
    "personal Expeneses.",
    "Entry fees",
    "Internation airfare.",
    "personal Expeneses.",
    "Entry fees",
    "Internation airfare.",
    "personal Expeneses.",
    "Entry fees",
    "Internation airfare.",
  ],
  costIncludes: [
    "Domestic traveling Expenses",
    "COmplete Accomodation",
    "Breakfast, Lunch and Dinner",
    "Domestic traveling Expenses",
    "COmplete Accomodation",
    "Breakfast, Lunch and Dinner",
    "Domestic traveling Expenses",
    "COmplete Accomodation",
    "Breakfast, Lunch and Dinner",
  ],
};

export const tempReviewsList = [
  {
    customerId: 1234567890,
    title: "THE ONLY COMPANY TO USE IN NEPAL",
    description:
      "Nepal has always held a fascination for me. So when I decided that I wanted to attempt the EBC trek, I began looking around for a company to provide me with what I needed. I found Nepal Hiking Team on Google and off I went.",
    rating: 5,
    publishedDate: 4 / 15 / 2020,
    displayOption: true,
  },
  {
    customerId: 2345678901,
    title: "THE BEST COMPANY TO USE IN NEPAL",
    description:
      "So when I decided that I wanted to attempt the EBC trek, I began looking around for a company to provide me with what I needed. I found Nepal Hiking Team on Google and off I went.",
    rating: 4,
    publishedDate: 4 / 15 / 2020,
    displayOption: true,
  },
  {
    customerId: 2345678901,
    title: "THE WORST COMPANY TO USE IN NEPAL",
    description:
      " I began looking around for a company to provide me with what I needed. I found Nepal Hiking Team on Google and off I went.",
    rating: 1,
    publishedDate: 4 / 15 / 2020,
    displayOption: false,
  },
  {
    customerId: 3456789012,
    title: "THE GOOD COMPANY TO USE IN NEPAL",
    description:
      "Nepal has always held a fascination for me. So when I decided that I wanted to attempt the EBC trek, I began looking around for a company to provide me with what I needed. I found Nepal Hiking Team on Google and off I went.",
    rating: 3,
    publishedDate: 4 / 15 / 2020,
    displayOption: true,
  },
  {
    customerId: 3456789012,
    title: "THE ONLY COMPANY TO USE IN NEPAL",
    description:
      "Nepal has always held a fascination for me. So when I decided that I wanted to attempt the EBC trek, I began looking around for a company to provide me with what I needed. I found Nepal Hiking Team on Google and off I went.",
    rating: 5,
    publishedDate: 4 / 15 / 2020,
    displayOption: false,
  },
];

export const tempCustomerDetail = [
  {
    _id: 1234567890,
    fullName: "Mark Zukerburg",
    gender: "Male",
    country: "USA",
    email: "mark@gmail.com",
    number: 1234567890,
    image: null,
    packageIds: [1, 2, 3],
  },
  {
    _id: 2345678901,
    fullName: "Bill Gates",
    gender: "Male",
    country: "UK",
    email: "bill@gmail.com",
    number: 2345678901,
    image: null,
    packageIds: [1, 2, 3],
  },
  {
    _id: 3456789012,
    fullName: "Jaquiline Fernandez",
    gender: "Female",
    country: "Sri Lanka",
    email: "jacq@gmail.com",
    number: 3456789012,
    image: null,
    packageIds: [1, 2, 3],
  },
];

export const tempContactDetail = [
  {
    title: "Head Office",
    personName: "Sizon Ghimire",
    address: "Baluwatar, kathmandu, Nepal",
    phoneNo: 977014567890,
    emailId: "aura.ktm@auravacation.com",
  },
  {
    title: "Pokhara  Office",
    personName: "Razani Singh Bam",
    address: "LakeSide, Pokhara, Nepal",
    phoneNo: 977129876543,
    emailId: "aura.pkr@auravacation.com",
  },
  {
    title: "Head Office",
    personName: "Sizon Ghimire",
    address: "Baluwatar, kathmandu, Nepal",
    phoneNo: 977014567890,
    emailId: "aura.ktm@auravacation.com",
  },
];

export const tempStyleDetail = {
  trek:
    "Amongst many activities in Nepal like day tours, jungle safari, adventure activities like bungee jumping, rafting, helicopter tours, trekking is the most famous one. Trekking in Nepal is the first option of anyone who would like to visit Nepal. Owning to its diversity and mountains in abundance, Nepal offers plenty of trekking options in different regions of Nepal. Different packages are designed looking after the need and preferences of customers. Packages varies in terms of preferred destination, time constraint, endurance capacity and type of adventure. There are easy treks for those who would love to immerse in the beauty of nature in an easy way. Similarly, moderate are for those who want a mix of adventure, travel, fun and natural beauty. Likewise, strenuous treks are for the real challenge.",
  tour:
    "Opt out to simple Nepal tours than those harsh and exhilarating trekking trails. Enjoy the natural beauty as well as cultural glory of Nepal as you visit different places of importance. Nepal is a country that holds pride of having diversity of people with their own unique culture and tradition. Nepal tour is an opportunity to get acquainted with unique caste, culture and creed of Nepal along with its lush and wild beauty.",
  expedition:
    "Opt out to simple Nepal tours than those harsh and exhilarating trekking trails. Enjoy the natural beauty as well as cultural glory of Nepal as you visit different places of importance. Nepal is a country that holds pride of having diversity of people with their own unique culture and tradition. Nepal tour is an opportunity to get acquainted with unique caste, culture and creed of Nepal along with its lush and wild beauty.",
  hikking:
    "Opt out to simple Nepal tours than those harsh and exhilarating trekking trails. Enjoy the natural beauty as well as cultural glory of Nepal as you visit different places of importance. Nepal is a country that holds pride of having diversity of people with their own unique culture and tradition. Nepal tour is an opportunity to get acquainted with unique caste, culture and creed of Nepal along with its lush and wild beauty.",
  adventureActivities:
    "Opt out to simple Nepal tours than those harsh and exhilarating trekking trails. Enjoy the natural beauty as well as cultural glory of Nepal as you visit different places of importance. Nepal is a country that holds pride of having diversity of people with their own unique culture and tradition. Nepal tour is an opportunity to get acquainted with unique caste, culture and creed of Nepal along with its lush and wild beauty.",
  mountainFlight:
    "Opt out to simple Nepal tours than those harsh and exhilarating trekking trails. Enjoy the natural beauty as well as cultural glory of Nepal as you visit different places of importance. Nepal is a country that holds pride of having diversity of people with their own unique culture and tradition. Nepal tour is an opportunity to get acquainted with unique caste, culture and creed of Nepal along with its lush and wild beauty.",
  heliTour:
    "Opt out to simple Nepal tours than those harsh and exhilarating trekking trails. Enjoy the natural beauty as well as cultural glory of Nepal as you visit different places of importance. Nepal is a country that holds pride of having diversity of people with their own unique culture and tradition. Nepal tour is an opportunity to get acquainted with unique caste, culture and creed of Nepal along with its lush and wild beauty.",
};
