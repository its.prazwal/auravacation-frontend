import React, {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import {tempCustomerDetail, tempReviewsList} from "../tempPackages";
import "./whyCompanyReviews.css";
import HttpCall from "../../utilities/Httpclient";
import {COMPANY, CUSTOMER, DIRECT_MAIL, REVIEWS} from "../../constants/ApiURL.constant";
import {useDispatch, useSelector} from "react-redux";
import {saveCompanyData} from "../../actions/Company.actions";
import {getReviews, saveCustomers, saveReviews} from "../../actions/Other.actions";

function WhyCompanyReviews({selected}) {
    const [reviewsList, setReviewsList] = useState([]);
    const [customerDetail, setCustomerDetail] = useState([]);
    const [toDisplayReview, setToDisplayReview] = useState({
        count: 0,
        data: {},
    });

    const dispatch = useDispatch();

    useEffect(() => {
        const enabledReviews = tempReviewsList.filter(
            (ele) => ele.displayOption === true
        );
        setReviewsList(enabledReviews);
        setCustomerDetail(tempCustomerDetail);
        const customer = tempCustomerDetail.filter(
            (cust) => cust._id === enabledReviews[0].customerId
        );
        setToDisplayReview({
            count: 0,
            data: {
                reviewTitle: enabledReviews[0].title,
                customerName: customer[0].fullName,
                customerCountry: customer[0].country,
                rating: enabledReviews[0].rating,
                reviewDescription: enabledReviews[0].description,
            },
        });
        HttpCall(COMPANY.WHY_COMPANY, 'GET', {})
            .then(data => dispatch(saveCompanyData(data, 'WHY_COMPANY')));
    }, []);


    const whyCompanyData = useSelector(state => state.company.WHY_COMPANY);
    const hasSelected = selected.length > 0;
    const whyCompanyToDisplay = hasSelected
        ? whyCompanyData.filter(wc => wc._id === selected[0].whyCompanyId)[0]
        : whyCompanyData[0];

    function chooseReviewDisplay(name) {
        document.getElementById('reviews-data').style.opacity = '0';
        const totalReviews = reviewsList.length - 1;
        let _count = toDisplayReview.count;
        if (name === "next") {
            _count === totalReviews ? (_count = 0) : (_count += 1);
        } else {
            _count === 0 ? (_count = totalReviews) : (_count -= 1);
        }
        const customer = customerDetail.filter(
            (cust) => cust._id === reviewsList[_count].customerId
        );
        setToDisplayReview({
            count: _count,
            data: {
                reviewTitle: reviewsList[_count].title,
                customerName: customer[0].fullName,
                customerCountry: customer[0].country,
                rating: reviewsList[_count].rating,
                reviewDescription: reviewsList[_count].description,
            },
        });
        setTimeout(()=>{
            document.getElementById('reviews-data').style.opacity = '1'
        },300)
    }

    let ratingStars = [];
    for (let index = 0; index < 5; index++) {
        ratingStars.push(
            <img
                key={index}
                src={require("./../../icons/ratingStar.ico")}
                alt="ratingStar.ico"
                className={`ratingStar ${
                    toDisplayReview.data.rating > index ? "star-active" : "star-inactive"
                }`}
            />
        );
    }
    return (
        <div className="whyCompanyReviews-wrapper disableTextSelection">
            <h1>Why Aura Vacation ?</h1>
            <div className="whyCompanyReviews-data-wrapper">
                <h3>{hasSelected && whyCompanyToDisplay.title}</h3>
                <p>
                    {hasSelected && whyCompanyToDisplay.description}
                    <br/>
                </p>
                <Link to="/Why-Aura-Vacation">more info...</Link>
            </div>
            <div className="whyCompanyReviews-data-wrapper">
                <span className='frw-previous-click'
                      onClick={()=>chooseReviewDisplay('previous')}>
                    {'<'}</span>
                <span className='frw-next-click'
                      onClick={()=>chooseReviewDisplay('next')}>
                    {'>'}</span>
                <h3>Testimonial / Reviews</h3>
                <div className="reviews-data" id='reviews-data'>
                    <h4>
                        {toDisplayReview.data.reviewTitle} <br/>
                        {toDisplayReview.data.customerName} | {ratingStars}
                    </h4>
                    <h6>{toDisplayReview.data.customerCountry}</h6>
                    <p>{toDisplayReview.data.reviewDescription}</p>
                    <Link to="/">read more...</Link>
                </div>
            </div>
        </div>
    );
}

export default WhyCompanyReviews;
