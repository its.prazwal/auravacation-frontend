import React, {useState, useEffect} from "react";
import "./footerBar.css";
import {logoUrl, logoSrc} from "../../common/imageUrl";
import {Link} from "react-router-dom";
import WhyCompanyReviews from "./whyCompanyReviews";
import HttpCall from "../../utilities/Httpclient";
import {COMPANY, FOOTER_LAYOUT, SUBSCRIBED} from "../../constants/ApiURL.constant";
import {showSuccess} from "../../utilities/notifier";
import {useDispatch, useSelector} from "react-redux";
import {saveCompanyData} from "../../actions/Company.actions";
import {saveFTLayout} from "../../actions/Layout.actions";

function FooterBar(props) {
    const [subscribeMail, setSubscribeMail] = useState("");
    const [logos, setLogos] = useState({
        associated: ["NTB", "TAAN", "NMA", "KEEP"],
        recommended: ["TripAdvisor"],
        followOn: ["Facebook", "Instagram"],
        weAccept: ["Visa", "MasterCard"],
    });
    const dispatch = useDispatch();
    useEffect(() => {
        HttpCall(COMPANY.CONTACT_DETAIL, 'GET', {})
            .then(data => {
                dispatch(saveCompanyData(data, 'CONTACT_DETAIL'));
            })
        HttpCall(FOOTER_LAYOUT, 'GET', {})
            .then(data => {
                let selected = [];
                if (data.length > 0) {
                    selected = data.filter(e => e.selected === true);
                }
                dispatch(saveFTLayout(selected));
            })
    }, []);

    const selected = useSelector(state => state.layouts.FOOTER);
    const contactDetails = useSelector(state => state.company.CONTACT_DETAIL);
    const hasSelected = selected.length > 0;
    const dataToDisplay = hasSelected ? contactDetails.filter(cd => {
        return selected[0].contactDetailId.some(cdi => cdi === cd._id) ? cd : null;
        }) : contactDetails

    function handleSubsChange(e) {
        setSubscribeMail(e.target.value);
    }

    function handleSubsSubmit(e) {
        e.preventDefault();
        HttpCall(SUBSCRIBED, 'POST', {body: {emailId: subscribeMail}})
            .then(data => {
                showSuccess(data);
                setSubscribeMail('');
            })
    }

    function logoHolder(logoOf, logoList) {
        return (
            <div className="logo-wrapper">
                <h2>{logoOf}</h2>
                <div className="logo-holder">
                    {logoList.map((logo, i) => {
                        return (
                            <Link to={logoUrl[logo]} key={i}>
                                <img
                                    src={logoSrc[logo]}
                                    alt={`${logo}.png`}
                                    className="footer-logo"
                                />
                            </Link>
                        );
                    })}
                </div>
            </div>
        );
    }

    const _length = hasSelected ? selected[0].contactDetailId.length : contactDetails.length < 4 ? contactDetails : 4;

    return (
        <>
            <WhyCompanyReviews selected={hasSelected ? selected : []}/>
            <div className="footer-data-1">
                {dataToDisplay.map((detail, i) => {
                    if (i < 4) {
                        return (
                            <div
                                className={`footer-contactDetail contactDetail0${_length}`}
                                key={i}
                            >
                                <div className="footer-contactDetail-data">
                                    <h2>{detail.officeName}</h2>
                                    <h4>{detail.contactPerson}</h4>
                                    <p>
                                        <b>Email:</b> {detail.emailId}
                                        <br/>
                                        <b>Phone:</b> {detail.phone}
                                        <br/>
                                        <b>Address:</b> {detail.address}
                                    </p>
                                </div>
                            </div>
                        );
                    }
                })}
                <div className="footer-contactUs">
                    <Link exact to='/Contact-Us'>
                        <button type="button" className="button">
                            DETAIL CONTACT INFO
                        </button>
                    </Link>
                </div>
            </div>
            <div className="footer-data-2 ">
                <div className="footer-data-2-text-1">
                    Sign up for <b>newsletters :</b>
                </div>
                <form onSubmit={handleSubsSubmit}>
                    <input
                        type="text"
                        name='emailId'
                        placeholder="Email Address to Subscribe"
                        onChange={handleSubsChange}
                        value={subscribeMail}
                    />
                    <button
                        className={`button button-${subscribeMail ? "edit" : "disabled"}`}
                        type="submit"
                        {...Object.assign({}, subscribeMail ? null : {disabled: true})}
                    >
                        Subscribe
                    </button>
                </form>
                <div className="footer-data-2-text-2 disableTextSelection">
                    Get News, Notifications, Discount, Offers and Updates about the recent
                    Events and Offers.
                </div>
            </div>
            <div className="footer-data-3 disableTextSelection">
                {logoHolder("Associated with", logos.associated)}
                {logoHolder("Recommended on", logos.recommended)}
                {logoHolder("Follow us on", logos.followOn)}
                {logoHolder("We Accept", logos.weAccept)}
            </div>
            <div className="footer-data-4 disabledTextSelection">
                Copyright ©. Aura Vacation Pvt. Ltd. All rights reserved.
            </div>
        </>
    );
}

export default FooterBar;
