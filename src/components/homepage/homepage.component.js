import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";

import HttpCall from "../../utilities/Httpclient";
import {PackageBox} from "../../common/packageBox.component";
import {savePackage} from '../../actions/Other.actions';

import * as API_URL from '../../constants/ApiURL.constant';
import * as CompanyActions from '../../actions/Company.actions';
import {DEFAULT, PACKAGE_IMG} from '../../constants/IMGURL.constant';

import "./homepage.css";
import {HP_LAYOUT} from "../../constants/ApiURL.constant";
import {saveHPLayout} from "../../actions/Layout.actions";

export function Homepage(props) {
    const dispatch = useDispatch();

    const aboutUsData = useSelector(state => state.company.ABOUT_US);
    const packageData = useSelector(state => state.package);
    const selectedLayout = useSelector(state => state.layouts.HOMEPAGE);
    const hasSelected = selectedLayout.length > 0;

    const selectedPack = hasSelected &&
        packageData.filter(pd => {
            return selectedLayout[0].sixPackagesId.some(spi => spi === pd._id) ? pd : null;
        })

    useEffect(() => {
        if (aboutUsData.length === 0 || packageData.length === 0) {
            refreshData();
        }
    }, []);


    function refreshData() {
        const companies = Object.keys(API_URL.COMPANY);
        companies.forEach((com) => {
            HttpCall(API_URL.COMPANY[com], 'GET', {})
                .then(data => {
                    dispatch(CompanyActions.saveCompanyData(data, com));
                })
        })
        HttpCall(API_URL.PACKAGE.COMMON, 'GET', {})
            .then(data => {
                dispatch(savePackage(data));
            })
        HttpCall(HP_LAYOUT, 'GET', {})
            .then(data => {
                let selected = [];
                if (data.length > 0) {
                    selected = data.filter(e => e.selected === true);
                }
                dispatch(saveHPLayout(selected));
            })
    }

    return (
        <div className="homepage-wrapper">
            <div className="homepage-data-1">
                <h1>
                    Aura <br/> Vacation
                </h1>
                <div className="homepage-img-1-wrapper" onClick={() => props.history.push('/style/trekking')}>
                    <h2>Trekking In Nepal</h2>
                    <img
                        src={hasSelected
                            ? selectedLayout[0].displayImage.trekking
                                ? `${PACKAGE_IMG}/${selectedLayout[0].displayImage.trekking}`
                                : DEFAULT.TREK_IMG
                            : DEFAULT.TREK_IMG}
                        alt="trekking-in-nepal.png"
                        className="homepage-img-1"
                    />
                </div>
                <div className="homepage-img-2-wrapper" onClick={() => props.history.push('/style/tour')}>
                    <h2>Tour In Nepal</h2>
                    <img
                        src={hasSelected
                            ? selectedLayout[0].displayImage.tour
                                ? `${PACKAGE_IMG}/${selectedLayout[0].displayImage.tour}`
                                : DEFAULT.TOUR_IMG
                            : DEFAULT.TOUR_IMG}
                        alt="tour-in-nepal.png"
                        className="homepage-img-2"
                    />
                </div>
            </div>
            <div className="homepage-data-2">
                <h1>About Us</h1>
                <p>{hasSelected
                    ? aboutUsData.filter(au => au._id === selectedLayout[0].aboutUsId)[0].description
                    : aboutUsData.length > 0 ? aboutUsData[0].description : 'One of the best Company.'}</p>
                <Link to="/About-Us">more info...</Link>
            </div>
            <div>
                <PackageBox
                    packageList={hasSelected
                        ? selectedPack : packageData}
                    boxTitle="Best Trekking and tour Packages"
                    history={props.history}
                />
            </div>
        </div>
    );
}
