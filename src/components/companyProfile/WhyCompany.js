import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import HttpCall from "../../utilities/Httpclient";
import {COMPANY} from "../../constants/ApiURL.constant";
import {saveCompanyData} from "../../actions/Company.actions";
import CompanyTopWrapper from "./Company.TopWrapper";

function WhyCompany() {
    const dispatch = useDispatch();
    useEffect(() => {
        HttpCall(COMPANY.WHY_COMPANY, 'GET', {})
            .then(data => dispatch(saveCompanyData(data, 'WHY_COMPANY')))
    }, []);
    const whyCompanyData = useSelector(state => state.company.WHY_COMPANY)

    return (
        <div>
            <CompanyTopWrapper profileName='Why Aura Vacation'/>
            <div className='why-company-detail-wrapper'>
                {whyCompanyData.length > 0 ? whyCompanyData.map((au) => {
                    return (
                        <div key={au._id} className='why-company-eachData'>
                            <h3>{au.title}</h3>
                            <p>{au.description}</p>
                        </div>
                    )
                }) : <div className='no-data'>
                    <h3>Why Aura Vacation</h3>
                    <p>Data is in progress...</p>
                </div>}
            </div>
        </div>
    )
}

export default WhyCompany;