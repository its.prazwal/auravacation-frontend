import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import HttpCall from "../../utilities/Httpclient";
import {COMPANY} from "../../constants/ApiURL.constant";
import {saveCompanyData} from "../../actions/Company.actions";
import CompanyTopWrapper from "./Company.TopWrapper";

function AboutUs() {
    const dispatch = useDispatch();
    useEffect(() => {
        HttpCall(COMPANY.ABOUT_US, 'GET', {})
            .then(data => dispatch(saveCompanyData(data, 'ABOUT_US')))
    }, []);
    const aboutUsData = useSelector(state => state.company.ABOUT_US)

    return (
        <div>
            <CompanyTopWrapper profileName='About Us'/>
            <div className='about-us-detail-wrapper'>
                {aboutUsData.length > 0 ? aboutUsData.map((au) => {
                    return (
                        <div key={au._id} className='about-us-eachData'>
                            <h3>{au.title}</h3>
                            <p>{au.description}</p>
                        </div>
                    )
                }) : <div className='no-data'>
                    <h3>About Us</h3>
                    <p>Data is in progress...</p>
                </div>}
            </div>
        </div>
    )
}

export default AboutUs;