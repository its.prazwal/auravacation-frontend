import React from "react";
import {COMPANY_LOGO} from "../../constants/IMGURL.constant";
import './CompanyProfile.css';

function CompanyTopWrapper({profileName}) {
    return (
        <div className='company-top-wrapper'>
            <div className='company-top-img-wrapper'>
                <img src={COMPANY_LOGO} alt='company.logo'/>
            </div>
            <div className='company-top-detail-wrapper'>
                <h2>AURA VACATION</h2>
                <p>Kathmandu, Nepal <br/> +977-1234567890</p>
            </div>
            <h1>{profileName}</h1>
        </div>
    )
}

export default CompanyTopWrapper;