import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import HttpCall from "../../utilities/Httpclient";
import {COMPANY} from "../../constants/ApiURL.constant";
import {saveCompanyData} from "../../actions/Company.actions";
import CompanyTopWrapper from "./Company.TopWrapper";
import {OUR_TEAM_IMG} from "../../constants/IMGURL.constant";

function OurTeam() {
    const dispatch = useDispatch();
    useEffect(() => {
        HttpCall(COMPANY.OUR_TEAM, 'GET', {})
            .then(data => {
                dispatch(saveCompanyData(data, 'OUR_TEAM'));
                               document.getElementById('our-team-detail-wrapper').style.height = `${data.length * 430}px`;
            })
    }, []);
    const ourTeamData = useSelector(state => state.company.OUR_TEAM)

    return (
        <div>
            <CompanyTopWrapper profileName='Our Team'/>
            <div className='our-team-detail-wrapper' id='our-team-detail-wrapper'>
                {ourTeamData.length > 0 ? ourTeamData.map((ot) => {
                    return (
                        <div key={ot._id} className='our-team-eachData'>
                            <img src={`${OUR_TEAM_IMG}/${ot.image}`}/>
                            <div className='our-team-eachData-detail'>
                                <h3>{ot.fullName}</h3>
                                <h4>{ot.designation}</h4>
                                <p>{ot.message}</p>
                            </div>
                        </div>
                    )
                }) : <div className='no-data'>
                    <h3>About Us</h3>
                    <p>Data is in progress...</p>
                </div>}
            </div>
        </div>
    )
}

export default OurTeam;