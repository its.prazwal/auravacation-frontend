import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import HttpCall from "../../utilities/Httpclient";
import {COMPANY} from "../../constants/ApiURL.constant";
import {saveCompanyData} from "../../actions/Company.actions";
import CompanyTopWrapper from "./Company.TopWrapper";

function PrivacyPolicy() {
    const dispatch = useDispatch();
    useEffect(() => {
        HttpCall(COMPANY.PRIVACY_POLICY, 'GET', {})
            .then(data => dispatch(saveCompanyData(data, 'PRIVACY_POLICY')))
    }, []);
    const privacyPolicyData = useSelector(state => state.company.PRIVACY_POLICY)

    return (
        <div>
            <CompanyTopWrapper profileName='Privacy and Policy'/>
            <div className='privacy-policy-detail-wrapper'>
                {privacyPolicyData.length > 0 ? privacyPolicyData.map((au) => {
                    return (
                        <div key={au._id} className='privacy-policy-eachData'>
                            <h3>{au.title}</h3>
                            <p>{au.description}</p>
                        </div>
                    )
                }) : <div className='no-data'>
                    <h3>Privacy Policy</h3>
                    <p>Data is in progress...</p>
                </div>}
            </div>
        </div>
    )
}

export default PrivacyPolicy;