import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import HttpCall from "../../utilities/Httpclient";
import {COMPANY} from "../../constants/ApiURL.constant";
import {saveCompanyData} from "../../actions/Company.actions";
import CompanyTopWrapper from "./Company.TopWrapper";
import {LEGAL_DOCS_IMG} from "../../constants/IMGURL.constant";
import {escapeListener} from "../../utilities/EventListener";

function LegalDocs() {
    const dispatch = useDispatch();
    const [viewImage, setViewImage] = useState({});

    useEffect(() => {
        HttpCall(COMPANY.LEGAL_DOCS, 'GET', {})
            .then(data => {
                dispatch(saveCompanyData(data, 'LEGAL_DOCS'));
                const wrapper = document.getElementById('legal-docs-wrapper')
                wrapper.style.height = `${parseInt((data.length + 3) / 3) * 620}px`;
            })
    }, []);
    const legalDocsData = useSelector(state => state.company.LEGAL_DOCS)

    function viewLegalDoc(ld) {
        setViewImage(ld);
        document.getElementById('legal-docs-view').style.display = 'block';
        setTimeout(() => {
            document.getElementById('legal-docs-content').style.opacity = '1';
        }, 100)
    }

    function hideLegalDoc() {
        document.getElementById('legal-docs-content').style.opacity = '0';
        setTimeout(() => {
            setViewImage({});
            document.getElementById('legal-docs-view').style.display = 'none';
        }, 700)
    }

    if (Object.keys(viewImage).length > 0) {
        escapeListener(hideLegalDoc);
    }

    return (
        <div>
            <div className='modal-box' id='legal-docs-view'>
                <div className='legal-docs-content' id='legal-docs-content'>
                    <span className='modal-close' onClick={hideLegalDoc}>&times;</span>
                    <h1>{viewImage.title}</h1>
                    {Object.keys(viewImage).length > 0 &&
                    <img src={`${LEGAL_DOCS_IMG}/${viewImage.docImage}`} alt='noImage'/>
                    }
                </div>
            </div>
            <CompanyTopWrapper profileName='Legal Documents'/>
            <div className='legal-docs-wrapper' id='legal-docs-wrapper'>
                {legalDocsData.length > 0 ? legalDocsData.map((ld) => {
                    return (
                        <div key={ld._id} className='legal-docs-eachData'
                             onClick={() => viewLegalDoc(ld)}
                        >
                            <h3>{ld.title}</h3>
                            <img src={`${LEGAL_DOCS_IMG}/${ld.docImage}`} alt='legal.doc'/>
                        </div>
                    )
                }) : <div className='no-data'>
                    <h3>About Us</h3>
                    <p>Data is in progress...</p>
                </div>}
            </div>
        </div>
    )
}

export default LegalDocs;