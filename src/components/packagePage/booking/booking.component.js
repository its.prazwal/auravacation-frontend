import React, {useEffect, useState} from "react";
import countriesISO from "iso3166-2-db/countryList/en.json";
import './booking.css';
import {PACKAGE_IMG} from "../../../constants/IMGURL.constant";
import HttpCall from "../../../utilities/Httpclient";
import {BOOKING, CUSTOMER} from "../../../constants/ApiURL.constant";
import {escapeListener} from "../../../utilities/EventListener";

const listOfCountries = Object.keys(countriesISO)
    .sort((a, b) =>
        countriesISO[a].name > countriesISO[b].name ? 1 : -1
    )
    .map(isoCode => {
        return countriesISO[isoCode].name
    })

const initialCustomerDetail = {
    fullName: "",
    emailId: "",
    phone: "",
    country: '',
};
const initialBookingDetail = {
    startDate: '',
    pax: 1,
    additionalInfo: '',
    specialReq: '',
};

const initialLogicState = {
    isSubmitting: false,
    bookingId: '',
    countDown: 10,
    isBooking: false,
}

function BookingComponent({chosenPackage}) {
    const [bookingDetail, setBookingDetail] = useState(initialBookingDetail);
    const [customerDetail, setCustomerDetail] = useState(initialCustomerDetail);
    const [logicState, setLogicState] = useState(initialLogicState);
    const [errors, setErrors] = useState({});

    useEffect(() => {
        if(chosenPackage)
        setLogicState(prevState => ({...prevState, isBooking: true}));
        if (bookingDetail.startDate && bookingDetail.pax) {
            const packageDetail = document.getElementById('booking-modal-package-detail').style;
            const packageConfirm = document.getElementById('booking-modal-package-confirm').style;
            packageDetail.height = '200px';
            packageDetail.padding = '10px';
            setTimeout(() => {
                packageConfirm.height = '80%';
                packageConfirm.padding = '10px';
            }, 1000)
        }
    }, [bookingDetail.pax, bookingDetail.startDate])

    if(logicState.isBooking){
        escapeListener(hideBookingForm);
    }

    function handleBookingChange(e) {
        let {name, value} = e.target;
        if (name === 'startDate' && new Date(value) < (Date.now() - 86400000)) {
            return null;
        }
        setBookingDetail(prevState => ({
            ...prevState,
            [name]: value,
        }))
        delete errors[name];
    }

    function handleCustomerChange(e) {
        const {name, value} = e.target;
        setCustomerDetail(prevState => ({
            ...prevState,
            [name]: value,
        }))
        delete errors[name];
    }

    function hideBookingForm() {
        setBookingDetail(initialBookingDetail);
        setLogicState(initialLogicState);
        document.getElementById('booking-modal-wrapper').style.display = 'none'
    }

    function validation() {
        let _errors = {};
        if (!bookingDetail.pax) _errors.pax = 'Number of people is required.';
        if (!bookingDetail.startDate) _errors.startDate = 'Trip start date is required.';
        if (!customerDetail.fullName) _errors.fullName = 'Full name is required.';
        if (!customerDetail.emailId) _errors.emailId = 'Email Address is required.';
        if (!customerDetail.country) _errors.country = 'Country is required.';
        setErrors(_errors);
        return Object.keys(_errors).length === 0;
    }

    function handleBookingSubmit(e) {
        e.preventDefault();
        if (validation()) {
            setLogicState(prevState => ({...prevState, isSubmitting: true}))
            HttpCall(CUSTOMER, 'POST', {body: customerDetail})
                .then(data => {
                    bookingDetail.customerId = data;
                    bookingDetail.packageId = chosenPackage._id;
                    HttpCall(BOOKING, 'POST', {body: bookingDetail})
                        .then(data => {
                            if(data === undefined){
                                setLogicState(prevState => ({...prevState,
                                isSubmitting: false}))
                            }
                            setTimeout(() => {
                                setLogicState(prevState => ({
                                    ...prevState,
                                    bookingId: data.bookingId,
                                    isSubmitting: false
                                }))
                                beginCountDown();
                            }, 1000);
                        })
                })
            setBookingDetail(initialBookingDetail);
            setCustomerDetail(initialCustomerDetail);
        }
    }

    function beginCountDown() {
        let _countDown = logicState.countDown;
        let interval = setInterval(() => {
            _countDown -= 1;
            console.log('_count: ', _countDown);
            setLogicState(prevState => ({...prevState, countDown: _countDown}))
            if (_countDown === 0) {
                clearInterval(interval);
                hideBookingForm();
            }
        }, 1000)
    }

    const _startDate = new Date(bookingDetail.startDate).toDateString();
    const timeStampStartDate = new Date(bookingDetail.startDate).getTime();
    const _endDate = new Date(timeStampStartDate + (86400000 * parseInt(chosenPackage.tripFacts.duration))).toDateString();
    return (
        <div className='modal-box' id='booking-modal-wrapper'>
            {logicState.isSubmitting || logicState.bookingId ?
                <div className='booking-modal-loader'>
                    {logicState.isSubmitting && <div className='loader'/>}
                    {logicState.bookingId &&
                    <div className='bookingId'>
                        <h4>Your Booking Id:</h4>
                        <h5 className='enableTextSelection'>{logicState.bookingId}</h5>
                        <p>Save it for future purposes. We have sent it to your email Id as well.</p>
                        This will close automatically in {logicState.countDown} seconds...
                    </div>
                    }
                </div>
                : null}
            <div className='booking-modal-content'>
                <h1>Booking
                    <span className='modal-close' onClick={hideBookingForm}>&times;</span>
                </h1>
                <div className='booking-modal-date-wrapper' id='booking-modal-date-wrapper'>
                    <h2>Trip Departure Date and Number of People</h2>
                    <label>Start Date: </label>
                    <input type='date' name='startDate'
                           onChange={handleBookingChange}
                           value={bookingDetail.startDate}
                           className={errors.startDate && 'input-error'}
                    />
                    <label>No. of Pax: </label>
                    <input typeof='number' name='pax'
                           className={`input-text ${errors.pax && 'input-error'}`}
                           onChange={handleBookingChange}
                           value={bookingDetail.pax}
                           maxLength={3}
                           placeholder={errors.pax || 'Number of people'}
                    />
                </div>
                {(bookingDetail.startDate && bookingDetail.pax) ?
                    <>
                        <div className='booking-modal-package-detail' id='booking-modal-package-detail'>
                            <h2>Package Detail</h2>
                            <img src={`${PACKAGE_IMG}/${chosenPackage.image}`}/>
                            <div className='booking-modal-pd-info'>
                                <h3>{chosenPackage.packageName} | {chosenPackage.country}</h3>
                                <li>Start Date: <b>{_startDate}</b> | End Date: <b>{_endDate}</b></li>
                                <li>Total
                                    Price: <b>USD {chosenPackage.tripFacts.price * (parseInt(bookingDetail.pax) || 0)}</b>
                                </li>
                                <li>Duration: <b>{chosenPackage.tripFacts.duration} Days</b></li>
                                <li>Number of People: <b>{bookingDetail.pax || 0}</b></li>
                            </div>
                        </div>
                        <div className='booking-modal-package-confirm' id='booking-modal-package-confirm'>
                            <h2>Customer Information</h2>
                            <form onSubmit={handleBookingSubmit}>
                                <label htmlFor="fullName">
                                    <i className="fas fa-user"/>
                                </label>
                                <input
                                    type="text"
                                    className={`input-text ${errors.fullName && 'input-error'}`}
                                    name="fullName"
                                    id="fullName"
                                    placeholder={errors.fullName || "Full Name"}
                                    onChange={handleCustomerChange}
                                    value={customerDetail.fullName}
                                />
                                <label htmlFor="emailId">
                                    <i className="fas fa-envelope"/>
                                </label>
                                <input
                                    type="text"
                                    className={`input-text ${errors.emailId && 'input-error'}`}
                                    name="emailId"
                                    id="emailId"
                                    placeholder={errors.emailId || "Email Address"}
                                    onChange={handleCustomerChange}
                                    value={customerDetail.emailId}
                                />
                                <br/>
                                <label htmlFor="phone">
                                    <i className="fas fa-phone-alt"/>
                                </label>
                                <input
                                    type="number"
                                    className="input-text"
                                    name="phone"
                                    id="phone"
                                    placeholder="Mobile No."
                                    onChange={handleCustomerChange}
                                    value={customerDetail.phone}
                                    maxLength={10}
                                />
                                <label htmlFor="country">
                                    <i className="fas fa-globe-asia"/> </label>
                                <select
                                    name="country"
                                    id="country"
                                    onChange={handleCustomerChange}
                                    className={errors.country && 'select-options-error'}
                                >
                                    <option disabled={true}
                                            selected={true}>{errors.country || "select country"}</option>
                                    {listOfCountries.map((c, i) => {
                                        return (<option key={i} value={c}>{c}</option>)
                                    })}
                                </select>
                                <br/>
                                <div className='booking-modal-textArea-wrapper'>
                                    <label htmlFor='specialReq'>Special Requirements:</label>
                                    <br/>
                                    <textarea
                                        type="text"
                                        className="input-textArea"
                                        name="specialReq"
                                        id="specialReq"
                                        placeholder="Special Requirements"
                                        onChange={handleBookingChange}
                                        value={bookingDetail.specialReq}
                                    />
                                </div>
                                <div className='booking-modal-textArea-wrapper'>
                                    <label htmlFor='additionalInfo'>Additional Information:</label>

                                    <textarea
                                        type="text"
                                        className="input-textArea"
                                        name="additionalInfo"
                                        id="additionalInfo"
                                        placeholder="Additional Information"
                                        onChange={handleBookingChange}
                                        value={bookingDetail.additionalInfo}
                                    />
                                </div>
                                <button
                                    type="submit"
                                    name="submit"
                                    className="rsb-buttons rsb-submitEnq"
                                >
                                    Submit
                                </button>
                            </form>
                        </div>
                    </> : null}
            </div>
        </div>
    )
}

export default BookingComponent;