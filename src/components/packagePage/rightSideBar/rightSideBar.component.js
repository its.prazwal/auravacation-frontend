import React, {useState} from "react";
import "./rightSideBar.css";
import BookingComponent from "../booking/booking.component";
import HttpCall from "../../../utilities/Httpclient";
import {DIRECT_MAIL} from "../../../constants/ApiURL.constant";
import * as notify from '../../../utilities/notifier';

const initialEnquiryDetail = {
    fullName: "",
    emailId: "",
    phone: "",
    message: "",
}

function RightSideBar({chosenPackage}) {
    const [enquiryDetail, setEnquiryDetail] = useState(initialEnquiryDetail);
    const [isSubmitting, setIsSubmitting] = useState(false);
    function handleQEchange(e) {
        const {name, value} = e.target;
        setEnquiryDetail(prevState => ({...prevState, [name]: value}));
    }

    function handleQEsubmit(e) {
        e.preventDefault();
        setIsSubmitting(true);
        HttpCall(DIRECT_MAIL, 'POST', {body: enquiryDetail })
            .then(data => {
                setTimeout(()=>{

                notify.showSuccess(data);
                setEnquiryDetail(initialEnquiryDetail);
                setIsSubmitting(false);
                },1000)
            })
    }

    function validation() {
        let _errors = {};
        if(!enquiryDetail.fullName) _errors.fullName = 'Full Name is required';
        if(!enquiryDetail.emailId) _errors.emailid = 'Email Id is required';
        return Object.keys(_errors).length === 0;
    }

    return (
        <>
            <BookingComponent chosenPackage={chosenPackage} />
            <div className="packagePage-rightSideBar-wrapper">
                <div className="rsb-priceWrapper">
                    <h1>Best Price</h1>
                    <div className="rsb-price">
                        <h4>All Inclusive Price</h4>
                        <h3>USD {chosenPackage.tripFacts.price}</h3>
                    </div>
                    <button
                        type="button"
                        name="bookTrip"
                        className="rsb-buttons rsb-booking"
                        onClick={() => {
                            document.getElementById('booking-modal-wrapper').style.display = 'block'
                        }}
                    >
                        Book Trip
                    </button>
                    <button
                        type="button"
                        name="customizeTrip"
                        className="rsb-buttons rsb-customize development-btn"
                        disabled={true}
                    >
                        customize Trip<br/>
                    </button>
                    <button
                        type="button"
                        name="downloadTrip"
                        className="rsb-buttons rsb-download development-btn"
                        disabled={true}
                    >
                        download<br/>
                    </button>
                </div>
                <div className="rsb-quickEnquiry">
                    {isSubmitting && <div className='rsb-loader'><div className='loader'/></div>}
                    <h1>Quick Enquiry</h1>
                    <form onSubmit={handleQEsubmit}>
                        <label htmlFor="fullName">
                            <i className="fas fa-user"/>
                        </label>
                        <input
                            type="text"
                            className="input-text"
                            name="fullName"
                            id="fullName"
                            placeholder="Full Name"
                            onChange={handleQEchange}
                            value={enquiryDetail.fullName}
                        />
                        <label htmlFor="emailId">
                            <i className="fas fa-envelope"/>
                        </label>
                        <input
                            type="text"
                            className="input-text"
                            name="emailId"
                            id="emailId"
                            placeholder="Email Address"
                            onChange={handleQEchange}
                            value={enquiryDetail.emailId}
                        />
                        <label htmlFor="phone">
                            <i className="fas fa-phone-alt"/>
                        </label>
                        <input
                            type="text"
                            className="input-text"
                            name="phone"
                            id="phone"
                            placeholder="Mobile No."
                            onChange={handleQEchange}
                            value={enquiryDetail.phone}
                        />
                        <textarea
                            type="text"
                            className="input-textArea"
                            name="message"
                            id="message"
                            placeholder="Message"
                            onChange={handleQEchange}
                            value={enquiryDetail.message}
                        />
                        <button
                            type="submit"
                            name="submit"
                            className={`rsb-buttons ${validation() ? 'rsb-submitEnq' : 'rsb-disabled'}`}
                            {...Object.assign({}, validation() ? null : {disabled: true})}
                        >
                            Send
                        </button>
                    </form>
                </div>
            </div>
</>
    );
}

export default RightSideBar;
