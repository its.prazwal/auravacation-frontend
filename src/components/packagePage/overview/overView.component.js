import React, {useState} from "react";
import "./overview.css";

function PackageOverview(props) {
    const [smtf, setSmtf] = useState(false);

    return (
        <>
            <div className="package-tripFacts">
                <h1>Trip Facts: </h1>
                <div className={`p-tf-wrapper ${smtf ? "show-facts" : "hide-facts"}`}>
                    {Object.keys(props.chosenPackage.tripFacts).map((fact, i) => {
                        if (Array.isArray(props.chosenPackage.tripFacts[fact]) === true) {
                            return (
                                <div key={i} className="p-tf-each">
                                    {fact}
                                    <br/>
                                    <b>
                                        {props.chosenPackage.tripFacts[fact].map((e, i) => {
                                            if (i === 0) return e;
                                            else return `, ${e}`;
                                        })}
                                    </b>
                                </div>
                            );
                        } else {
                            return (
                                <div key={i} className="p-tf-each">
                                    {fact}
                                    <br/>
                                    <b>
                                        {fact === "price" && "USD "}
                                        {props.chosenPackage.tripFacts[fact]}{" "}
                                        {fact === "duration" && "Days"}
                                        {fact === "altitude" && "meters"}
                                    </b>
                                </div>
                            );
                        }
                    })}
                </div>
                <button
                    className="button"
                    onClick={() => (smtf ? setSmtf(false) : setSmtf(true))}
                >
                    {smtf ? "less facts" : "more facts"}
                    <div {...Object.assign({}, smtf ? {className: "arrow-up"} : null)}>
                        <i className="fas fa-chevron-down"/>
                    </div>
                </button>
            </div>
            <div className="package-highlights">
                <h1>Trip Highlights:</h1>
                <div className="p-hl-data">
                    {props.chosenPackage.highlights.map((hl, i) => {
                        return <p key={i}>{hl}</p>;
                    })}
                </div>
            </div>
            <div className="package-description">
                <h1>Short Description</h1>
                <p>{props.chosenPackage.description}</p>
            </div>
            <div className="package-shortItinerary">
                <h1>Short Itinerary</h1>
                {props.chosenPackage.itinerary.map((iti, i) => {
                    return (
                        <p key={i}>
                            <b>Day {iti.day}: </b> {iti.title}
                        </p>
                    );
                })}
            </div>
        </>
    );
}

export default PackageOverview;
