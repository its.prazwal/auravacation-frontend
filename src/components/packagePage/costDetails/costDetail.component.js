import React, {useState} from "react";
import "./costDetail.css";

function PackageCostDetail(props) {
    const [whichD, setWhichD] = useState("includes");
    return (
        <div className="package-costDetail-wrapper">
            <div className="package-costDetail-buttons">
                <button
                    type="button"
                    name="includes"
                    className={`pcd-button ${
                        whichD === "includes" && "pcd-ci-btn-act"
                    }`}
                    onClick={() => setWhichD("includes")}
                >
                    Includes
                </button>
                <button
                    type="button"
                    name="excludes"
                    className={`pcd-button ${
                        whichD === "excludes" && "pcd-ce-btn-act"
                    }`}
                    onClick={() => setWhichD("excludes")}
                >
                    Excludes
                </button>
            </div>
            <div
                className={`package-costDetail-data ${
                    whichD === "includes" ? "pcd-ci" : "pcd-ce"
                }`}
            >

                <ul>
                    {props.costDetail[whichD].map((det, i) => {
                        return <li key={i}>{det}</li>;
                    })}
                </ul>
            </div>
        </div>
    );
}

export default PackageCostDetail;
