import React, {useState, useEffect} from "react";
import PackageOverview from "./overview/overView.component";
import PackageCostDetail from "./costDetails/costDetail.component";
import PackageItinerary from "./itinerary/itinerary.component";
import PackageReviews from "./reviews/reviews.component";
import PackageUsefulInfo from "./usefulInfo/usefulInfo.component";
import RightSideBar from "./rightSideBar/rightSideBar.component";
import {PackageBox} from "../../common/packageBox.component";
import "./packagePage.css";
import {useDispatch, useSelector} from "react-redux";
import HttpCall from "../../utilities/Httpclient";
import {PACKAGE} from "../../constants/ApiURL.constant";
import {savePackage} from "../../actions/Other.actions";
import {PACKAGE_IMG} from "../../constants/IMGURL.constant";

function PackagePage(props) {
    const [chosenPackage, setChosenPackage] = useState([]);
    const [showingComponent, setShowing] = useState();
    const _packageId = props.match.params.id;

    const dispatch = useDispatch();
    const packageList = useSelector(state => state.package);

    useEffect(() => {
        if (packageList.length === 0) {
            HttpCall(PACKAGE.COMMON, 'GET', {})
                .then(data => dispatch(savePackage(data)));
        }
        setChosenPackage(packageList.filter((pack) => pack._id === _packageId));
        scrollToTop();
    }, [_packageId, packageList]);
    let relatedPackage = [];
    if (chosenPackage.length !== 0) {
        relatedPackage = packageList.filter(pack => (pack.style === chosenPackage[0].style)
            && (pack._id !== chosenPackage[0]._id));
    }

    useEffect(() => {
        chosenPackage.length > 0 &&
        setShowing(<PackageOverview chosenPackage={chosenPackage[0]}/>);
    }, [chosenPackage]);

    function scrollToTop() {
        const c = document.documentElement.scrollTop || document.body.scrollTop;
        if (c > 0) {
            window.requestAnimationFrame(scrollToTop);
            window.scrollTo(0, c - c / 15);
        }
    };

    function changeShowingComp(e) {
        const {id} = e.target;
        const pdwElement = document.getElementsByClassName(
            "packageDetail-wrapper"
        )[0];
        pdwElement.scrollIntoView();

        switch (id) {
            case "overview":
                setShowing(<PackageOverview chosenPackage={chosenPackage[0]}/>);
                break;
            case "costDetail":
                setShowing(
                    <PackageCostDetail costDetail={chosenPackage[0].costDetails}/>
                );
                break;
            case "detailItinerary":
                setShowing(<PackageItinerary itinerary={chosenPackage[0].itinerary}/>);
                break;
            case "usefulInfo":
                setShowing(<PackageUsefulInfo/>);
                break;
            case "reviews":
                setShowing(<PackageReviews/>);
                break;
            default:
                break;
        }
    }

    return (
        <>
            {chosenPackage.length > 0 ? (
                <div className="packagePage-wrapper">
                    <div className="packagePage-topImage">
                        <h1>{chosenPackage[0].packageName}</h1>
                        <img
                            src={`${PACKAGE_IMG}/${chosenPackage[0].image}`}
                            alt={chosenPackage[0].image}
                        />
                    </div>
                    <div className="packageDetail-wrapper">
                        <div className="packageDetail-navBar">
                            <div className="packageDetail-buttons">
                                {['overview', 'detailItinerary', 'costDetail', 'usefulInfo', 'reviews'].map((e, i) => {
                                    return (

                                        <button
                                            className="button"
                                            onClick={changeShowingComp}
                                            id={e}
                                            key={i}
                                        >
                                            {e.replace(/^\w/, (c) => c.toUpperCase())}
                                        </button>
                                    )
                                })}
                            </div>
                            <div className="packageDetail-title">
                                <h1>{chosenPackage[0].packageName}</h1>
                            </div>
                        </div>
                        <RightSideBar chosenPackage={chosenPackage[0]}/>
                        <div className="packageDetail-data-wrapper">{showingComponent}</div>
                    </div>
                    <PackageBox
                        packageList={relatedPackage}
                        boxTitle="Related Packages"
                        history={props.history}
                    />
                </div>
            ) : (
                <div className="unknownPackage">unknown package</div>
            )}
        </>
    );
}

export default PackagePage;
