import React from "react";
import "./itinerary.css";

function PackageItinerary(props) {
  return (
    <div className="package-itinerary-data">
      <h1>Detailed Itinerary</h1>
      {props.itinerary.map((iti, i) => {
        return (
          <div className="itinerary-each">
            <h3>
              Day {iti.day}: <b>{iti.title}</b>
            </h3>
            <p>{iti.detail}</p>
          </div>
        );
      })}
    </div>
  );
}

export default PackageItinerary;
