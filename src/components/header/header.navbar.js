import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {SearchBar} from "../searchPackage/searchBar";
import HttpCall from "../../utilities/Httpclient";
import * as API_URL from "../../constants/ApiURL.constant";
import {saveTripFacts} from "../../actions/TripFacts.actions";
import {COMPANY_LOGO} from "../../constants/IMGURL.constant";

import "./header.css";

export function HeadBar(props) {
    const [searching, setSearching] = useState(false);
    const dispatch = useDispatch();

    function closeSearchBar(e) {
        setSearching(false);
        enableScroll();
    }
    function enableScroll() {
        window.onscroll = () => {
        };
    }

    useEffect(() => {
        HttpCall(API_URL.TRIP_FACTS.STYLES, 'GET', {})
            .then(data => {
                dispatch(saveTripFacts(data, 'STYLES'));
            })
    }, []);
    const styles = useSelector(state => state.tripFacts.STYLES);

    return (
        <>
            {searching ? (
                <SearchBar
                    closeSearchBar={closeSearchBar}
                    history={props.history}
                />
            ) : (
                <>
                    <Link to="/" className="headBar-links">
                        <i className="fa fa-home" aria-hidden="true"/>
                    </Link>
                    {styles ? styles.map((sty, i) => {
                        const _name = sty.name.replace(" ", "-").toLowerCase();
                        return (
                            <Link to={`/style/${_name}`} className="headBar-links" key={i}>
                                {sty.name}
                            </Link>
                        )
                    }): null}
                    <button
                        name="searchButton"
                        type="button"
                        className="headBar-search-button"
                        onClick={() => setSearching(true)}
                    >
                        <i className="fas fa-search"/>
                    </button>
                    <div className='headBar-logo'>
                        <img src={COMPANY_LOGO} alt='company.png'/>
                        <div className='headBar-logo-dropDown'>
                            <h3>Company Profile</h3>
                            {['About Us', 'Contact Us', 'Legal Documents', 'Our Team', 'Privacy Policy', 'Why Aura Vacation'].map
                            ((ele, i) => {
                                const _forLink = ele.replace(/\s/g, "-");
                                return (
                                    <Link to={`/${_forLink}`} className='headBar-links' key={i}>
                                        <button>{ele}</button>
                                    </Link>
                                )
                            })}
                        </div>
                    </div>
                </>
            )}
        </>
    );
}
