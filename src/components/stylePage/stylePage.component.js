import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";

import HttpCall from "../../utilities/Httpclient";
import {PackageBox} from "../../common/packageBox.component";
import {savePackage} from "../../actions/Other.actions";
import {saveTripFacts} from "../../actions/TripFacts.actions";
import {PACKAGE, TRIP_FACTS} from "../../constants/ApiURL.constant";
import {PACKAGE_IMG} from "../../constants/IMGURL.constant";

import "./stylePage.css";

function StylePageComponent(props) {
    const chosenStyle = props.match.params.name.replace('-', ' ').toUpperCase();
    const dispatch = useDispatch();

    const styles = useSelector(state => state.tripFacts.STYLES);
    const packageList = useSelector(state => state.package);

    useEffect(() => {
        scrollToTop();
        if (styles.length === 0) {
            HttpCall(TRIP_FACTS.STYLES, 'GET', {})
                .then(data => dispatch(saveTripFacts(data, 'STYLES')))
        }
        if (packageList.length === 0) {
            HttpCall(PACKAGE.COMMON, 'GET', {})
                .then(data => dispatch(savePackage(data)))
        }
    }, [chosenStyle]);
    const styleDetails = styles.filter(sty => sty.name.toUpperCase() === chosenStyle)[0];
    const relatedPackage = packageList.filter(pack => pack.style.toUpperCase() === chosenStyle);

    function scrollToTop() {
        const c = document.documentElement.scrollTop || document.body.scrollTop;
        if (c > 0) {
            window.requestAnimationFrame(scrollToTop);
            window.scrollTo(0, c - c / 15);
        }
    };

    return (
        <div className="stylePage-wrapper">
            <div className="stylePage-data-1">
                <h1 className="stylePage-title">
                    <b>{chosenStyle}</b> in Nepal
                </h1>

                <div
                    id="stylePageCarousel"
                    className="carousel slide"
                    data-ride="carousel"
                    data-interval="3000"
                    data-pause="false"
                >
                    <ol className="carousel-indicators">
                        {relatedPackage.map((pack, i) => {
                            return (
                                <li
                                    key={i}
                                    data-target="#stylePageCarousel"
                                    data-slide-to={i}
                                    {...Object.assign(
                                        {},
                                        i === 0 ? {className: "active"} : null
                                    )}
                                />
                            );
                        })}
                    </ol>

                    <div className="carousel-inner" role="listbox">
                        {relatedPackage.map((pack, i) => {
                            return (
                                <div
                                    key={i}
                                    className={`item ${i === 0 ? "active" : null}`}
                                    id="stylePageCarousel-image-wrapper"
                                >
                                    <img
                                        className="stylePageCarousel-image"
                                        src={`${PACKAGE_IMG}/${pack.image}`}
                                        alt={pack.image}
                                    />
                                </div>
                            );
                        })}
                    </div>
                    <a
                        className="left carousel-control"
                        href="#stylePageCarousel"
                        role="button"
                        data-slide="prev"
                    >
            <span
                className="glyphicon glyphicon-chevron-left"
                aria-hidden="true"
            />
                        <span className="sr-only">Previous</span>
                    </a>
                    <a
                        className="right carousel-control"
                        href="#stylePageCarousel"
                        role="button"
                        data-slide="next"
                    >
            <span
                className="glyphicon glyphicon-chevron-right"
                aria-hidden="true"
            />
                        <span className="sr-only">Next</span>
                    </a>
                </div>
                <p className="stylePage-shortDes">
                    {styleDetails && styleDetails.detail.split(".")[0]}.
                </p>
            </div>
            <div className="stylePage-data-2">
                <h3>Short Summary on {chosenStyle} in Nepal</h3>
                <p>{styleDetails && styleDetails.detail}</p>
            </div>
            <div className="stylePage-data-3">
                <PackageBox
                    packageList={relatedPackage}
                    boxTitle="Best selling Packages"
                    history={props.history}
                />
            </div>
        </div>
    );
}

export default StylePageComponent;
