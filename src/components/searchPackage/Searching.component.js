function SearchingComponent(data, packageList) {
    console.log("package: ", packageList);
    console.log('data: ', data);
    // country, region, adventureAct, packageName, style, duration
    let searchData = [];
    packageList.forEach(pl => {
        const newRegex = new RegExp(`${data.toLowerCase()}`, 'g');
        console.log('packageName:', pl.packageName.toLowerCase().match(newRegex));
        console.log('style:', pl.style.toLowerCase().match(newRegex));
        console.log('region:', pl.region.toLowerCase().match(newRegex));
        console.log('adventureType:', pl.adventureType.toLowerCase().match(newRegex));
        console.log('country:', pl.country.toLowerCase().match(newRegex));
    })
}

export default SearchingComponent