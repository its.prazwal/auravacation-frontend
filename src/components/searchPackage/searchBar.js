import React, {useState, useEffect} from "react";
import {DEFAULT} from "../../constants/IMGURL.constant";
import {useDispatch, useSelector} from "react-redux";
import HttpCall from "../../utilities/Httpclient";
import {PACKAGE} from "../../constants/ApiURL.constant";
import {savePackage} from "../../actions/Other.actions";
import SearchingComponent from "./Searching.component";

export function SearchBar(props) {
    const [searchData, setSearchData] = useState([]);
    const dispatch = useDispatch();

    useEffect(() => {
        HttpCall(PACKAGE.COMMON, 'GET', {})
            .then(data => dispatch(savePackage(data)));
        disableScroll();
    }, []);

    function disableScroll() {
        // Get the current page scroll position
        const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        const scrollLeft = window.pageXOffset || document.documentElement.scrollLeft;
        window.onscroll = () => {
            return window.scrollTo(scrollLeft, scrollTop);
        };
    }

    const packageList = useSelector(state => state.package)

    function handleChange(e) {
        if (e.target.value.length > 2)
            SearchingComponent(e.target.value, packageList);
    }

    function handleSubmit(e) {
        e.preventDefault();
        console.log("showing");
    }

    return (
        <div className="searchBar-wrapper">
            <div className="searchBar-head">
                <button
                    className=" headBar-search-button float-left"
                    onClick={() => document.getElementById("searchField").focus()}
                >
                    <i className="fas fa-search"/>
                </button>
                <form onSubmit={handleSubmit}>
                    <input
                        id="searchField"
                        type="text"
                        name="searchField"
                        className="headBar-search-input"
                        autoFocus
                        placeholder="Search localhost:3006"
                        onChange={handleChange}
                    />
                </form>
                <button
                    className="headBar-search-button searchBar-close"
                    onClick={props.closeSearchBar}
                >
                    <span>&times;</span>
                </button>
            </div>

            <div className="searchBar-body">
                {searchData.length > 0 ? <h5>Top Result</h5> : null}
                {searchData.map((sd, i) => {
                    if (i < 3) {
                        return packageList.map((pack) => {
                            return sd[0] === pack._id ? (
                                <div
                                    className="searchBar-data"
                                    key={pack._id}
                                    onClick={() => {
                                        props.history.push(`/package/${pack._id}`);
                                        props.closeSearchBar();
                                    }}
                                >
                                    <img src={DEFAULT.TOUR_IMG} alt="temp.png"/>
                                    <h4>{pack.title}</h4>
                                    <h6>
                                        {pack.tripFacts.duration - 1} Nights{" "}
                                        {pack.tripFacts.duration} Days | ${pack.tripFacts.price}
                                    </h6>
                                    <h6>
                                        {pack.country} |{" "}
                                    </h6>
                                </div>
                            ) : null;
                        });
                    } else {
                        return null;
                    }
                })}
            </div>
        </div>
    );
}
