import {notify} from 'react-notify-toast';

export function showSuccess(msg){
    notify.show(msg, 'success', 3000);
}