import Axios from "axios";

const http = Axios.create({
    baseURL: 'http://localhost:2020/api',
    responseType: 'json',
})

const bodyHeader = {
    'content-type': 'application/json'
}

async function HttpCall(url, method, {params = {}, body = {}, responseType = 'json'} = {}) {
    return await  http({
        method,
        url,
        headers: bodyHeader,
        data: body,
        params,
        responseType,
    }).then(data => {
         return data.data;
    }).catch(err=> console.trace("err: ",err))
}

export default HttpCall;