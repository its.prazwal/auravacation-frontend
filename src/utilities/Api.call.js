import React from "react";
import {useDispatch} from "react-redux";
import HttpCall from "./Httpclient";
import * as API_URL from '../constants/ApiURL.constant';

import * as CompanyActions from '../actions/Company.actions';

function ImportCompany() {
    const dispatch = useDispatch();
    const companies = Object.keys(API_URL.COMPANY);
    companies.forEach((com) => {
        HttpCall(API_URL.COMPANY[com], 'GET', {})
            .then(data => {
                dispatch(CompanyActions.saveCompanyData(data, com));
            })
    })
}

export default ImportCompany;