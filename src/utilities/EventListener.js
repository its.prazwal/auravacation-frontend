export function escapeListener(func) {
    document.addEventListener('keydown', escapeEvent);
    function escapeEvent(evt) {
        if (evt.key === 'Escape') {
            func();
            document.removeEventListener('keydown', escapeEvent);
            document.removeEventListener('keydown', escapeEvent);
        }
    }
}