import React from "react";
import "./App.css";
import ComponentRouter from "./app.routing";
import "./common/css/cssLoader";
import Notifications from 'react-notify-toast';

function App() {
  return (
    <div>
      <ComponentRouter/>
      <Notifications />
    </div>
  );
}

export default App;
