export function saveCompanyData(data, subHead) {
    return {
        type: `SAVE_${subHead}`,
        payload: data
    }
}

export function getAboutUs() {
    return {type: `GET_ABOUT_US`}
}
export function getContactUs() {
    return {type: `GET_CONTACT_DETAIL`}
}

export function getLegalDocs() {
    return {type: `GET_LEGAL_DOCS`}
}

export function getOurTeam() {
    return {type: `GET_OUR_TEAM`}
}

export function getPrivacyPolicy() {
    return {type: `GET_PRIVACY_POLICY`}
}

export function getWhyCompany() {
    return {type: `GET_WHY_COMPANY`}
}