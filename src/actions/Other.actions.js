//ADVENTURE ACTIVITY
export function saveAdventureAct(data) {
    return {
        type: 'SAVE_ADVENTURE_ACT',
        payload: data,
    }
}

export function getAdventureAct() {
    return {type: 'GET_ADVENTURE_ACT'}
}

//PACKAGE
export function savePackage(data) {
    return {
        type: 'SAVE_PACKAGE',
        payload: data,
    }
}

export function getPackage() {
    return {type: 'GET_PACKAGE'}
}

//REVIEWS
export function saveReviews(data) {
    return {
        type: 'SAVE_REVIEWS',
        payload: data,
    }
}

export function getReviews() {
    return {type: 'GET_REVIEWS'}
}

//CUSTOMERS
export function saveCustomers(data) {
    return {
        type: 'SAVE_CUSTOMERS',
        payload: data,
    }
}

//USEFUL INFORMATION
export function saveUsefulInfo(data) {
    return {
        type: 'SAVE_USEFUL_INFO',
        payload: data,
    }
}

export function getUsefulInfo() {
    return {type: 'GET_USEFUL_INFO'}
}