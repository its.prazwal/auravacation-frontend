export function saveTripFacts(data, subHead) {
    return {
        type: `SAVE_${subHead}`,
        payload: data
    }
}

export function getAccommodation() {
    return {type: 'GET_ACCOMMODATION'}
}

export function getHighlights() {
    return {type: 'GET_HIGHLIGHTS'}
}

export function getStyles() {
    return {type: 'GET_STYLES'}
}

export function getTransportation() {
    return {type: 'GET_TRANSPORTATION'}
}