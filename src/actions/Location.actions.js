export function saveLocationData(data, subHead) {
    return {
        type: `SAVE_${subHead}`,
        payload: data,
    }
}

export function getCountry() {
    return {type: 'GET_COUNTRY'}
}

export function getPlaces() {
    return {type: 'GET_PLACES'}
}

export function getRegion() {
    return {type: 'GET_REGION'}
}