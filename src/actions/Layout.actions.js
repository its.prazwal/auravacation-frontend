export function saveHPLayout(data){
    return{
        type: 'SAVE_HP_LAYOUT',
        payload: data
    }
}

export function getHPLayout(){
    return{
        type: 'GET_HP_LAYOUT'
    }
}

export function saveFTLayout(data){
    return{
        type: 'SAVE_FT_LAYOUT',
        payload: data
    }
}

export function getFTLayout(){
    return{
        type: 'GET_FT_LAYOUT'
    }
}