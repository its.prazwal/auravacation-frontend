export function saveCostDetailData(data, subHead) {
    return {
        type: `SAVE_${subHead}`,
        payload: data
    }
}

export function getExcludes() {
    return {type: `GET_EXCLUDES`}
}

export function getIncludes() {
    return {type: `GET_INCLUDES`}
}
